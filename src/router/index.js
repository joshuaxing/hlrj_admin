/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-30 23:05:41
 * @LastEditTime: 2019-08-26 20:03:35
 * @LastEditors: Please set LastEditors
 */
import Vue from 'vue'
import Router from 'vue-router'
import store from '@/vuex/store';
Vue.use(Router);
let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/merchant'
    },
    {
      path: '/',
      component: resolve => require(['../components/admin/Home/index.vue'], resolve),
      meta: {
        requiresAuth: true
      },
      children: [{
          path: '/merchant',
          name: 'merchant',
          component: resolve => require(['../components/admin/Merchant/base.vue'], resolve),
          meta: {
            title: '基本信息',
            requiresAuth: true
          }
        },
        {
          path: '/order',
          name: 'merchantorderlist',
          component: resolve => require(['../components/admin/Merchant/order.vue'], resolve),
          meta: {
            title: '订单列表',
            requiresAuthValue: true
          },
        },
        {
          path: '/orderadd',
          name: 'merchantorderadd',
          component: resolve => require(['../components/admin/Merchant/orderadd.vue'], resolve),
          meta: {
            title: '订单新增',
            requiresAuthValue: true
          },
        },
        {
          path: '/orderedit',
          name: 'merchantorderedit',
          component: resolve => require(['../components/admin/Merchant/orderedit.vue'], resolve),
          meta: {
            title: '订单编辑',
            requiresAuthValue: true
          },
        },
         {
           path: '/ordercat',
           name: 'merchantordercat',
           component: resolve => require(['../components/admin/Merchant/ordercat.vue'], resolve),
           meta: {
             title: '订单详情',
             requiresAuthValue: true
           },
         },
        {
          path: '/siyi',
          name: 'merchantsiyi',
          component: resolve => require(['../components/admin/Merchant/siyi.vue'], resolve),
          meta: {
            title: '司仪列表',
            requiresAuthValue: true
          },
        },
        {
          path: '/sheying',
          name: 'merchantsheying',
          component: resolve => require(['../components/admin/Merchant/sheying.vue'], resolve),
          meta: {
            title: '摄影列表',
            requiresAuthValue: true
          },
        },
        {
          path: '/genzhuang',
          name: 'merchantgenzhuang',
          component: resolve => require(['../components/admin/Merchant/genzhuang.vue'], resolve),
          meta: {
            title: '跟妆列表',
            requiresAuthValue: true
          },
        },
        {
          path: '/coupon',
          name: 'merchantcoupon',
          component: resolve => require(['../components/admin/Merchant/coupon.vue'], resolve),
          meta: {
            title: '营销活动',
            requiresAuthValue: true
          },
        },
        {
          path: '/huiche',
          name: 'carhuiche',
          component: resolve => require(['../components/admin/Merchant/huiche.vue'], resolve),
          meta: {
            title: '婚车管理',
            requiresAuthValue: true
          }
        },
        {
          path: '/check',
          name: 'merchantcheck',
          component: resolve => require(['../components/admin/Merchant/check.vue'], resolve),
          meta: {
            title: '审核列表',
            requiresAuth: true
          },
        },
        {
          path: '/product',
          name: 'merchantproduct',
          component: resolve => require(['../components/admin/Merchant/product.vue'], resolve),
          meta: {
            title: '产品列表',
            requiresAuthValue: true
          }
        }, {
          path: '/case',
          name: 'merchantcase',
          component: resolve => require(['../components/admin/Merchant/case.vue'], resolve),
          meta: {
            title: '案例列表',
            requiresAuthValue: true
          },
        },
        {
          path: '/suggest',
          name: 'suggest',
          component: resolve => require(['../components/admin/Merchant/suggest.vue'], resolve),
          meta: {
            title: '商家建议',
            requiresAuthValue: true
          },
        },
      ]
    },
    {
      path: '/regist',
      name: 'regist',
      component: resolve => require(['../components/admin/Regist/index.vue'], resolve)
    }, {
      path: '/login',
      name: 'login',
      component: resolve => require(['../components/admin/Regist/login.vue'], resolve)
    }, {
      path: '/forget',
      name: 'forget',
      component: resolve => require(['../components/admin/Regist/forget.vue'], resolve)
    },
    {
      path: '/nopower',
      name: 'nopower',
      component: resolve => require(['../components/admin/Regist/nopower.vue'], resolve)
    },
    {
      path: '*',
      redirect: '/404'
    }, {
      path: '/404',
      component: resolve => require(['../components/page/404.vue'], resolve)
    },
    {
      path: '/test',
      component: resolve => require(['../components/test.vue'], resolve)
    }
  ]
})
router.beforeEach((to, from, next) => {
  if (sessionStorage.getItem('hlrjlogin')) {
    store.commit({
      type: 'uulogin',
      data: JSON.parse(sessionStorage.getItem('hlrjlogin'))
    })
  }
  //管理后台
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    let data = store.state.uulogin
    //console.log(data)
    if (data.id) {
      next()
    } else {
      next({
        path: '/login'
      })
    }
  } else {
    next() // 确保一定要调用 next()
  }
  //商家
  if (to.matched.some(record => record.meta.requiresAuthValue)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    let value = store.state.uulogin
    //console.log(data)
    if (value.backupstatus === 2) {
      next()
    } else {
      next({
        path: '/nopower'
      })
    }
  } else {
    next() // 确保一定要调用 next()
  }
})
export default router
