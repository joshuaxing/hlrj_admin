/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-07-30 23:05:41
 * @LastEditTime: 2019-08-26 20:03:14
 * @LastEditors: Please set LastEditors
 */
import Vuex from 'vuex'
import Vue from 'vue'
Vue.use(Vuex);

let sidebarMerchant = [
  {
    icon: "icon-dianpu",
    url: "",
    name: "婚庆店中心",
    id: "1",
    children: [{
      id: "2",
      icon: "",
      url: "/merchant",
      name: "基本信息",
      children: []
    }]
  },
  {
    icon: "icon-chanpin4",
    url: "",
    name: "产品管理",
    id: "3",
    children: [{
      id: "4",
      icon: "",
      url: "/product",
      name: "产品列表",
      children: []
    }]
  },
  {
    icon: "icon-anli3",
    url: "",
    name: "案例管理",
    id: "6",
    children: [{
      id: "7",
      icon: "",
      url: "/case",
      name: "案例列表",
      children: []
    }]
  },
  {
    icon: "icon-siyi1",
    url: "",
    name: "司仪管理",
    id: "10",
    children: [{
      id: "11",
      icon: "",
      url: "/siyi",
      name: "司仪列表",
      children: []
    }]
  },
  {
    icon: "icon-sheying",
    url: "",
    name: "摄影管理",
    id: "12",
    children: [{
      id: "13",
      icon: "",
      url: "/sheying",
      name: "摄影列表",
      children: []
    }]
  },
  {
    icon: "icon-huazhuang",
    url: "",
    name: "跟妆管理",
    id: "14",
    children: [{
      id: "15",
      icon: "",
      url: "/genzhuang",
      name: "跟妆列表",
      children: []
    }]
  },
  {
    icon: "icon-huodong4",
    url: "",
    name: "营销管理",
    id: "16",
    children: [{
      id: "17",
      icon: "",
      url: "/coupon",
      name: "营销活动",
      children: []
    }]
  },
  {
    icon: "icon-hunchezulin",
    url: "",
    name: "婚车管理",
    id: "26",
    children: [{
      icon: "",
      url: "/huiche",
      name: "婚车列表",
      id: "27",
      children: []
    }]
  },
  {
    icon: "icon-baogaogongdan",
    url: "",
    name: "订单管理",
    id: "24",
    children: [{
      id: "25",
      icon: "",
      url: "/order",
      name: "订单列表",
      children: []
    }]
  },
  {
    icon: "icon-shenhe1",
    url: "",
    name: "我的审核",
    id: "21",
    children: [{
      id: "22",
      icon: "",
      url: "/check",
      name: "审核列表",
      children: []
    }, {
      id: "28",
      icon: "",
      url: "/suggest",
      name: "商家建议",
      children: []
    }]
  }
]

let store = new Vuex.Store({
  state: {
    uulogin: {
      name: '',
      account: '',
      id: 0,
      type: 0,
      logoName: '',
      province: '',
      city: '',
      area: ''
    },
    tagsList: [],
    collapse: false,
    sidebarMenus: []
  },
  getters: {

  },
  mutations: {
    uulogin(state, payload) {
      let data = payload.data;
      state.sidebarMenus = sidebarMerchant
      state.uulogin = {
        ...data,
        logoName: '商户管理系统'
      };
    },
    tags(state, payload) {
      const data = payload.value;
      let arr = [];
      for (let i = 0, len = data.length; i < len; i++) {
        data[i].name && arr.push(data[i].name);
      }
      state.tagsList = arr;
    }
  }
})

export default store
